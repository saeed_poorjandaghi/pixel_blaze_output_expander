| Pixle Blaze Output Expander Input | Teensy / Power Supply |
| --------------------------------- | --------------------- |
| DAT                               | 1                     |
| CLK                               | -                     |
| GND                               | GND                   |
| 5V                                | +5V                   |



| Pixle Blaze Output Expander Output | LED Strip |
| ---------------------------------- | --------- |
| DATA                               | DATA      |
| 5V                                 | -         |
| GND                                | GND       |

