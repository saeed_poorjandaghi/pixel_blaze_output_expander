#include <Arduino.h>
#include "PBDriverAdapter.hpp"

PBDriverAdapter driver;

const int testChannelCount = 8;
const int pixelsPerChannel = 49;

void setup() {
  // put your setup code here, to run once:
  std::unique_ptr<std::vector<PBChannel>> channels(new std::vector<PBChannel>(testChannelCount));

  for (int i = 0; i < testChannelCount; i++) {
    (*channels)[i].channelId = i;
    (*channels)[i].channelType = CHANNEL_WS2812;
    (*channels)[i].numElements = 3;
    (*channels)[i].redi = 1;
    (*channels)[i].greeni = 0;
    (*channels)[i].bluei = 2;
    (*channels)[i].pixels = pixelsPerChannel;
    (*channels)[i].startIndex = i * pixelsPerChannel;
    (*channels)[i].frequency = 800000;
  }

  driver.configureChannels(std::move(channels));
  driver.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  unsigned long ms = millis();
  int channelId = 0;

  uint8_t rgb[3] = {0, 0, 0};

  driver.show(pixelsPerChannel * testChannelCount, [&](uint16_t index, uint8_t rgbv[]) {
    uint8_t hue = channelId * 32;
    float v = (1 + sinf(fmodf((ms + channelId*100) / 160.0, PI)))/2;
    v = v*v;
  
    if (hue < 85) {
      rgb[0] = hue * 3 * v;
      rgb[1] = (255 - hue * 3) * v;
      rgb[2] = 0;
    } else if (hue < 170) {
      hue -= 85;
      rgb[0] = (255 - hue * 3) * v;
      rgb[1] = 0;
      rgb[2] = hue * 3 * v;
    } else {
      hue -= 170;
      rgb[0] = 0;
      rgb[1] = hue * 3 * v;
      rgb[2] = (255 - hue * 3) * v;
    }
    
    memcpy(rgbv, rgb, 3);
  }, [&](PBChannel * ch) {
    channelId = ch->channelId;
  });
}